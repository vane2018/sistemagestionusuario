package com.softlogia.usuarios.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class Usuario implements Serializable {
  private static final long serialVersionUID = -5386261105143954755L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer usuarioId;
  @Column(name = "nombre")
  private String nombre;
  @Column(name = "apellido")
  private String apellido;
  @Column(name = "dni")
  private String dni;

  public Usuario() {
  }

  /**
   *
   * @param usuarioId
   * @param nombre
   * @param apellido
   * @param dni
   */
  public Usuario(Integer usuarioId, String nombre, String apellido, String dni) {
    this.usuarioId = usuarioId;
    this.nombre = nombre;
    this.apellido = apellido;
    this.dni = dni;
  }

  public Integer getUsuarioId() {
    return usuarioId;
  }

  public void setUsuarioId(Integer usuarioId) {
    this.usuarioId = usuarioId;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getApellido() {
    return apellido;
  }

  public void setApellido(String apellido) {
    this.apellido = apellido;
  }

  public String getDni() {
    return dni;
  }

  public void setDni(String dni) {
    this.dni = dni;
  }
}
