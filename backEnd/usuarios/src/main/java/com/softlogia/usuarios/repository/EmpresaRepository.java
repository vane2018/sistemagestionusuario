package com.softlogia.usuarios.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.softlogia.usuarios.entity.Empresa;

public interface EmpresaRepository extends JpaRepository <Empresa,Integer>{

}
