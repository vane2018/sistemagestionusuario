package com.softlogia.usuarios.repository;

import com.softlogia.usuarios.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author jlopez
 */
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

}
