package com.softlogia.usuarios.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.softlogia.usuarios.entity.Empresa;

import com.softlogia.usuarios.service.EmpresaService;
import com.softlogia.usuarios.web.rest.util.HeaderUtil;
import com.softlogia.usuarios.web.rest.util.ResponseUtil;



@RestController
@RequestMapping("/api")
public class EmpresaResource {
	
	private final Logger log= (Logger) LoggerFactory.getLogger(EmpresaResource.class);
     private static final String ENTITY_NAME="usuario";	
     private final EmpresaService empresaService;
     
     public EmpresaResource(EmpresaService empresaService) {
    	 this.empresaService=empresaService;
     }
     
     @GetMapping("/empresas")
     public ResponseEntity<List<Empresa>> getAllProductos(){
    	  log.debug("REST request to get a page of Productos");
    	    List<Empresa> page = empresaService.findAll();
    	    return new ResponseEntity<>(page, HttpStatus.OK);
     }
     
     @PostMapping("/empresas")
     public ResponseEntity<Empresa> createProducto(@RequestBody Empresa empresa) throws URISyntaxException {
       log.debug("REST request to save Empresa : {}", empresa);

       Empresa result = empresaService.save(empresa);
       return ResponseEntity.created(new URI("/api/empresas/" + result.getIdEmpresa()))
           .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdEmpresa().toString()))
           .body(result);
     }

     @PutMapping("/usuarios")
     public ResponseEntity<Empresa> updateProducto(@RequestBody Empresa usuario) throws URISyntaxException {
       log.debug("REST request to update Producto : {}", usuario);

       Empresa result = empresaService.save(usuario);
       return ResponseEntity.ok()
           .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, usuario.getIdEmpresa().toString()))
           .body(result);
     }

     @GetMapping("/empresas/{empresaId}")
     public ResponseEntity<Empresa> getProducto(@PathVariable Integer empresaId) {
       log.debug("REST request to get Empresa : {}", empresaId);
       Optional<Empresa> productoDTO = empresaService.findOne(empresaId);
       return ResponseUtil.wrapOrNotFound(productoDTO);
     }

     @DeleteMapping("/empresass/{empresaId}")
     public ResponseEntity<Void> deleteEmpresa(@PathVariable Integer empresaId) {
       log.debug("REST request to delete Producto : {}", empresaId);
       empresaService.delete(empresaId);
       return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, empresaId.toString())).build();
     }

}
