package com.softlogia.usuarios.service;

import com.softlogia.usuarios.entity.Usuario;
import java.util.List;
import java.util.Optional;

public interface UsuarioService {

  Usuario save(Usuario usuario);

  List<Usuario> findAll();

  Optional<Usuario> findOne(Integer usuarioId);

  void delete(Integer usuarioId);
}
