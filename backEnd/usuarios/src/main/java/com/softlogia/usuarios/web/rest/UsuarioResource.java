package com.softlogia.usuarios.web.rest;

import com.softlogia.usuarios.entity.Usuario;
import com.softlogia.usuarios.service.UsuarioService;
import com.softlogia.usuarios.web.rest.util.HeaderUtil;
import com.softlogia.usuarios.web.rest.util.ResponseUtil;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UsuarioResource {

  private final Logger log = LoggerFactory.getLogger(UsuarioResource.class);

  private static final String ENTITY_NAME = "usuario";

  private final UsuarioService usuarioService;

  public UsuarioResource(UsuarioService usuarioService) {
    this.usuarioService = usuarioService;
  }

  /**
   *
   * @return
   */
  @GetMapping("/usuarios")
  public ResponseEntity<List<Usuario>> getAllProductos() {
    log.debug("REST request to get a page of Productos");
    List<Usuario> page = usuarioService.findAll();
    return new ResponseEntity<>(page, HttpStatus.OK);
  }

  @PostMapping("/usuarios")
  public ResponseEntity<Usuario> createProducto(@RequestBody Usuario usuario) throws URISyntaxException {
    log.debug("REST request to save Producto : {}", usuario);

    Usuario result = usuarioService.save(usuario);
    return ResponseEntity.created(new URI("/api/productos/" + result.getUsuarioId()))
        .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getUsuarioId().toString()))
        .body(result);
  }

  @PutMapping("/usuarios")
  public ResponseEntity<Usuario> updateProducto(@RequestBody Usuario usuario) throws URISyntaxException {
    log.debug("REST request to update Producto : {}", usuario);

    Usuario result = usuarioService.save(usuario);
    return ResponseEntity.ok()
        .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, usuario.getUsuarioId().toString()))
        .body(result);
  }

  @GetMapping("/usuarios/{usuarioId}")
  public ResponseEntity<Usuario> getProducto(@PathVariable Integer usarioId) {
    log.debug("REST request to get Producto : {}", usarioId);
    Optional<Usuario> productoDTO = usuarioService.findOne(usarioId);
    return ResponseUtil.wrapOrNotFound(productoDTO);
  }

  @DeleteMapping("/usuarios/{usuarioId}")
  public ResponseEntity<Void> deleteProducto(@PathVariable Integer usuarioId) {
    log.debug("REST request to delete Producto : {}", usuarioId);
    usuarioService.delete(usuarioId);
    return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, usuarioId.toString())).build();
  }
}
