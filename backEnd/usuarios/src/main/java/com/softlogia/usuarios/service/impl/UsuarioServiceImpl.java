package com.softlogia.usuarios.service.impl;

import com.softlogia.usuarios.entity.Usuario;
import com.softlogia.usuarios.repository.UsuarioRepository;
import com.softlogia.usuarios.service.UsuarioService;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jlopez
 */
@Service
@Transactional
public class UsuarioServiceImpl implements UsuarioService {

  private final UsuarioRepository usuarioRepository;

  public UsuarioServiceImpl(UsuarioRepository usuarioRepository) {
    this.usuarioRepository = usuarioRepository;
  }

  @Override
  public Usuario save(Usuario usuario) {
    return this.usuarioRepository.save(usuario);
  }

  @Override
  public List<Usuario> findAll() {
    return this.usuarioRepository.findAll();
  }

  @Override
  public Optional<Usuario> findOne(Integer usuarioId) {
    return this.usuarioRepository.findById(usuarioId);
  }

  @Override
  public void delete(Integer usuarioId) {
    this.usuarioRepository.deleteById(usuarioId);
  }
}
