package com.softlogia.usuarios.service;

import java.util.List;
import java.util.Optional;

import com.softlogia.usuarios.entity.Empresa;


public interface EmpresaService {
	
	Empresa save(Empresa empresa);
	Optional<Empresa> findOne(Integer empresaId);
	 List<Empresa> findAll();
	
	void delete(Integer empresaId);

}
